import { ApiProperty } from "@nestjs/swagger";
import { IsEmail, IsNotEmpty, MinLength } from "class-validator";
import { RoleType } from "src/shared/enum/role-type.enum";
import { PrimaryColumn } from "typeorm";

export class CreateUserDto {
    @IsNotEmpty()
    @MinLength(3)
    @ApiProperty()
    @PrimaryColumn()
    username: string;

    @IsNotEmpty()
    @MinLength(8)
    @ApiProperty()
    password: string;

    @IsNotEmpty()
    @IsEmail()
    @PrimaryColumn()
    @ApiProperty()
    email: string;

    @IsNotEmpty()
    @ApiProperty()
    role: RoleType;
}