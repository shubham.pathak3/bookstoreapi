import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { validate } from 'class-validator';
import { User } from 'src/typeorm';
import { Repository } from 'typeorm';
import { CreateUserDto } from './dtos/create-user';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {}

  // createUser(createUserDto: CreateUserDto) {
  //   const newUser = this.userRepository.create(createUserDto);
  //   return this.userRepository.save(newUser);
  // }

  async createUser(body: CreateUserDto): Promise<Record<string, any>> {
    // Validation Flag
    let isOk = false;

    body.password = bcrypt.hashSync(body.password, 10);

    await validate(body).then((errors) => {
      if (errors.length > 0) {

      } else {
        isOk = true;
      }
    });
    if (isOk) {
      await this.userRepository.save(body).catch((error) => {
        isOk = false;
      });
      if (isOk) {
        return { status: 201, content: { msg: `User created with success` } };
      } else {
        return { status: 400, content: { msg: 'User already exists' } };
      }
    } else {
      return { status: 400, content: { msg: 'Invalid content' } };
    }
  }

  getUsers() {
    return this.userRepository.find({ relations: ['posts'] });
  }

  //   findUsersById(id: number) {
  //     return this.userRepository.findOne(id);
  //   }
}
