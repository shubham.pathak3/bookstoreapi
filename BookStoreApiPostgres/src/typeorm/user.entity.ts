import { Exclude } from 'class-transformer';
import { RoleType } from 'src/shared/enum/role-type.enum';
import { Column, Entity, JoinColumn, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import {  Posts } from './post.entity';

@Entity()
export class User {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'user_id',
  })
  id: number;
  
  @Column({
    nullable: false,
    default: '',
  })
  username: string;

  @Column({
    name: 'email_address',
    nullable: false,
    default: '',
    unique:true
  })
  email: string;

  @Column({
    nullable: false,
    default: '',
  })
  @Exclude()
  password: string;
  @Column({
    nullable: false,
    default: '',
  })
  role: RoleType;
  @OneToMany(() => Posts, (post: Posts) => post.Author)
  @JoinColumn()
  public posts: Posts[];
}