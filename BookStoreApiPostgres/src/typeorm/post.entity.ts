import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { User } from './user.entity';

@Entity()
export class Posts {
  @PrimaryGeneratedColumn()
  id: number;
  
  @Column({
    nullable: false,
    default: '',
  })
  Title: string;

  @Column({
    nullable: false,
    default: '',
  })
  Content: string;

  @Column({
    nullable: false,
    default: '',
  })
  Category: string;

  @ManyToOne(() => User, (author: User) => author.posts)
  Author: User;
}