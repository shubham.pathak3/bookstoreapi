import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';

@Entity()
export class Book extends BaseEntity {
  @PrimaryGeneratedColumn({
    type: 'bigint',
    name: 'book_id',
  })
  id: number;
  
  @Column({
    nullable: false,
    default: '',
  })
  BookName: string;

  @Column({
    nullable: false,
    default: '',
  })
  Author: string;

  @Column({
    nullable: false,
    default: '',
  })
  Price: string;
  @Column({
    nullable: false,
    default: false,
  })
  IsPublished: boolean;

  @Column({
    nullable: true
  })
  imageUrl: string;

  @CreateDateColumn()
  CreatedAt: Date;

  @UpdateDateColumn()
  UpdatedAt: Date;
}