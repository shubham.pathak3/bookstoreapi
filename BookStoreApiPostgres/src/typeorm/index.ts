
import { Book } from "./book.entity";
import { Posts } from "./post.entity";
import { User } from "./user.entity";

const entities = [User,Book,Posts];

export {User,Book,Posts};
export default entities;