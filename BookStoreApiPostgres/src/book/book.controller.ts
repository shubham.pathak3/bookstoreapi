import { Body, Controller, DefaultValuePipe, Delete, Get, Param, ParseIntPipe, Post, Put, Query, UploadedFile, UseGuards, UseInterceptors, UsePipes, ValidationPipe } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { HasRoles } from 'src/auth/guard/has-role.decorator';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guard/role.guard';
import { RoleType } from 'src/shared/enum/role-type.enum';
import { Book } from 'src/typeorm/book.entity';
import { BookService } from './book.service';
import { BookDto } from './dtos/book.dto';
import { PaginationParams } from './dtos/paginationParams.dto';

@Controller('book')
@ApiTags('book')
export class BookController {
  constructor(private bookService: BookService) { }

  @Get('')
  //@ApiBearerAuth('Authorization')
  //@UseGuards(JwtAuthGuard)
  @UsePipes(new ValidationPipe({ transform: true }))
  getAll(
    @Query() pagination?: PaginationParams,
  ): Promise<Book[]> {
    return this.bookService.getAll(pagination);
  }
  @Get(':id')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard)
  getBookById(@Param('id') id: number): Promise<Book> {
    return this.bookService.findById(id);
  }
  @Put(':id')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HasRoles(RoleType.ADMIN)
  updateBook(
    @Param('id') id: number,
    @Body() post: BookDto,
  ): Promise<any> {
    return this.bookService.update(id, post)
  }

  @Post('')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HasRoles(RoleType.ADMIN)
  @UsePipes(ValidationPipe)
  AddBook(
    @Body() book: BookDto
  ): Promise<Book> {

    return this.bookService.save(book)
  }

  @Delete(':id')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HasRoles(RoleType.ADMIN)
  deleteBook(
    @Param('id') id: number,
  ): Promise<any> {
    return this.bookService.delete(id)
  }

  @Put('uploadImage/:id')
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    schema: {
      type: 'object',
      properties: {
        file: {
          type: 'string',
          format: 'binary',
        },
      },
    },
  })
  //@UseInterceptors(FileExtender)
  @UseInterceptors(FileInterceptor('file'))
  uploadFile(@Param('id') id: number,@UploadedFile('file') file) {
   return this.bookService.uploadImage(id,file)
  }

  

}


