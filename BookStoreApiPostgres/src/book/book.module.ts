import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Book } from 'src/typeorm/book.entity';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import {S3Service} from '../s3/s3.service'

@Module({
    imports: [TypeOrmModule.forFeature([Book])],
    controllers: [BookController],
    providers: [BookService,S3Service]
  })
export class BookModule {}
