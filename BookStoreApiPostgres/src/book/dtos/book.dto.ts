import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';
export class BookDto {

  @IsNotEmpty()
  @ApiProperty()
  readonly BookName:string;
  @IsNotEmpty()
  @ApiProperty()
  readonly Author:string;
  @IsNotEmpty()
  @ApiProperty()
  readonly Price:string;
  @IsNotEmpty()
  @ApiProperty()
  readonly IsPublished:boolean;
}