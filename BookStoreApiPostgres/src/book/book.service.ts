import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Book } from 'src/typeorm/book.entity';
import { Like, Repository } from 'typeorm';
import { BookDto } from './dtos/book.dto';
import { PaginationParams } from './dtos/paginationParams.dto';
import {S3Service} from '../s3/s3.service'

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(Book) private readonly bookRepository: Repository<Book>,
    private s3 : S3Service
  ) {}

  save(createBookDto: BookDto) {
    const newBook = this.bookRepository.create(createBookDto);
    return this.bookRepository.save(newBook);
  }

  getAll(pagination:PaginationParams):Promise<Book[]> {
    return this.bookRepository.find({skip: pagination.skip, take: pagination.limit});
  }

  async findById(id: number) {
    const book = await this.bookRepository.findOneBy({ id })
    if (book) {
      return book;
    }
    else {
      throw new NotFoundException("Not Found");
    }
  }
  delete(id: number) {
    return this.bookRepository.delete({ id })
  }

  async update(id: number, data: BookDto) {
    const book = await this.bookRepository.findOneBy({ id })
    if (book){
      return this.bookRepository.update({ id }, data)
    }
    else
      throw new NotFoundException("Not Found");

  }

  async uploadImage(id:number,file){
    const book = await this.bookRepository.findOneBy({ id })
    if(book){
     const res =  await this.s3.uploadFile(file);
     return this.bookRepository.update({id},{...book,imageUrl:res.Location})
  
    }
  }

}
