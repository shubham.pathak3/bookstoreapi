import { Request } from 'express';
import { UserPrincipal } from './user-principal';

export interface AuthenticatedRequest extends Request {
 readonly user: UserPrincipal;
}