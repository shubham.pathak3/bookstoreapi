import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { validate } from 'class-validator';
import { User } from 'src/typeorm';
import { UsersDTO } from 'src/users/dtos/user.dto';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import * as nodemailer from 'nodemailer';
import {
  AuthenticationDetails,
  CognitoUser,
  CognitoUserPool,
  CognitoUserAttribute,
} from 'amazon-cognito-identity-js';
import { RegisterUserDto } from './dtos/register-user.dto';
import { Email } from './dtos/email';

@Injectable()
export class AuthService {
  private userPool: CognitoUserPool;
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
    private jwtService: JwtService,
  ) {
    this.userPool = new CognitoUserPool({
      UserPoolId: process.env.USERPOOLID,
      ClientId: process.env.CLIENTID,
    });
  }

  async login(user: any): Promise<Record<string, any>> {
    // Validation Flag
    let isOk = false;

    // Transform body into DTO
    const userDTO = new UsersDTO();
    userDTO.email = user.email;
    userDTO.password = user.password;

    // Validate DTO against validate function from class-validator
    await validate(userDTO).then((errors) => {
      if (errors.length > 0) {
      } else {
        isOk = true;
      }
    });

    if (isOk) {
      // Get user information
      const userDetails = await this.userRepository.findOneBy({
        email: user.email,
      });
      if (userDetails == null) {
        return { status: 401, msg: { msg: 'Invalid credentials' } };
      }

      // Check if the given password match with saved password
      const isValid = bcrypt.compareSync(user.password, userDetails.password);
      if (isValid) {
        return {
          status: 200,
          msg: {
            email: userDetails.email,
            access_token: this.jwtService.sign({
              email: userDetails.email,
              role: userDetails.role,
              id: userDetails.id,
            }),
          },
        };
      } else {
        return { status: 401, msg: { msg: 'Invalid credentials' } };
      }
    } else {
      return { status: 400, msg: { msg: 'Invalid fields.' } };
    }
  }

  registerUser(registerRequest: RegisterUserDto) {
    const { name, email, password } = registerRequest;
    return new Promise((resolve, reject) => {
      return this.userPool.signUp(
        email,
        password,
        [new CognitoUserAttribute({ Name: 'email', Value: email })],
        null,
        (error, result) => {
          if (!result) {
            reject(error);
          } else {
            const email = {
              subject: 'Account Created Successfully',
              body: 'Account is successfully created',
              from: 'pathakshubham692@gmail.com',
              to: 'pathakshubham692@gmail.com',
            };
            this.sendMail(email);
            resolve(result.user);
          }
        },
      );
    });
  }

  async authenticateUser(user: UsersDTO) {
    const { email, password } = user;

    const authenticationDetails = new AuthenticationDetails({
      Username: email,
      Password: password,
    });
    const userData = {
      Username: email,
      Pool: this.userPool,
    };

    const newUser = new CognitoUser(userData);

    return new Promise((resolve, reject) => {
      return newUser.authenticateUser(authenticationDetails, {
        onSuccess: (result) => {
          resolve(result);
        },
        onFailure: (err) => {
          reject(err);
        },
      });
    });
  }

  public async sendMail(email: Email) {
    const mailerClient: nodemailer.Transporter = nodemailer.createTransport({
      host: process.env.SES_HOST,
      port: process.env.SES_PORT,
      auth: {
        user: process.env.SES_USER,
        pass: process.env.SES_PASS,
      },
    });

    // send email
    console.log('Email Sent');
    return mailerClient.sendMail({
      from: email.from,
      to: email.to,
      subject: email.subject,
      html: email.body,
    });
  }
}
