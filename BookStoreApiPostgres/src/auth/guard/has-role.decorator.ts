import { SetMetadata } from '@nestjs/common';
import { RoleType } from '../../shared/enum/role-type.enum';

export const HasRoles = (...args: RoleType[]) => SetMetadata('has-role', args);