import { CanActivate, ExecutionContext, Injectable } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { Observable } from 'rxjs';
import { RoleType } from 'src/shared/enum/role-type.enum';
//import { RoleType } from '../../shared/enum/role-type.enum';
import { AuthenticatedRequest } from '../interface/authenticated-request';

@Injectable()
export class RolesGuard implements CanActivate {
  constructor(private readonly reflector: Reflector) {}
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const role = this.reflector.get<RoleType[]>(
      'has-role',
      context.getHandler(),
    );
    if (!role || role.length == 0) {
      return true;
    }

    const {
      user,
    } = context.switchToHttp().getRequest() as AuthenticatedRequest;
    return user.role && role.some((r) => r==user.role);
  }
}