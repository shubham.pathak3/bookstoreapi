import { BadRequestException, Body, Controller, Post, Req, Res, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { UsersDTO } from 'src/users/dtos/user.dto';
import { AuthService } from './auth.service';
import { RegisterUserDto } from './dtos/register-user.dto';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
    constructor(private authService: AuthService) { }

    @Post('login')
    @UsePipes(ValidationPipe)
    async login( @Body() body: UsersDTO) {
       
        try {
            return await this.authService.authenticateUser(body);
        } catch (ex) {
            throw new BadRequestException(ex.message);
        }
      //  res.status(auth.status).json(auth.msg);
    }
    @Post('create')
    @UsePipes(ValidationPipe)
    async createUsers(@Body() createUserDto: RegisterUserDto) {
        try {
            return await this.authService.registerUser(createUserDto);
        } catch (ex) {
            throw new BadRequestException(ex.message);
        }
    }
}
