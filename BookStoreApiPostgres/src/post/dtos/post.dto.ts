import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';
export class PostDto {

  @IsNotEmpty()
  @ApiProperty()
  readonly Title:string;
  @IsNotEmpty()
  @ApiProperty()
  readonly Content:string;
  @IsNotEmpty()
  @ApiProperty() 
  readonly Category:string;
}