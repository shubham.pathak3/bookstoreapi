import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { User } from 'src/typeorm';
import { Posts } from 'src/typeorm/post.entity';
import { Repository } from 'typeorm';
import { PostDto } from './dtos/post.dto';

@Injectable()
export class PostService {

  constructor(
    @InjectRepository(Posts) private readonly postRepository: Repository<Posts>,
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) { }

  async save(createPostDto: PostDto, user: User) {
    user = await this.userRepository.findOneBy({ id: user.id });
    const newPost = await this.postRepository.create({ ...createPostDto, Author: user });
    return this.postRepository.save(newPost);
  }

  async get() {
    return await this.postRepository.find({ relations: ['Author'] });
  }

  async update(id: number, post: PostDto) {
   const extPost = await this.postRepository.findOneBy({ id });
    if (extPost) {
      return  this.postRepository.update({ id }, post)
    } else {
      throw new NotFoundException('Not Found');
    }

  }

}
