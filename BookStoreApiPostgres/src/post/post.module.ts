import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from 'src/typeorm';
import {  Posts } from 'src/typeorm/post.entity';
import { PostController } from './post.controller';
import { PostService } from './post.service';

@Module({
    imports: [TypeOrmModule.forFeature([Posts,User])],
    controllers: [PostController],
    providers: [PostService]
  })
export class PostModule {}
