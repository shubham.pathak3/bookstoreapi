import { Body, Controller, Get, Param, Post, Put, Req, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { Posts } from 'src/typeorm/post.entity';
import { PostDto } from './dtos/post.dto';
import { PostService } from './post.service';

@Controller('post')
@ApiTags('Post')
export class PostController {
  constructor(private postService: PostService) { }


  @Post('')
  @UsePipes(ValidationPipe)
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard)
  AddPost(@Req() req,
    @Body() post: PostDto
  ): Promise<Posts> {
    return this.postService.save(post, req.user)
  }
  @Put(':id')
  @UsePipes(ValidationPipe)
  @ApiBearerAuth('Authorization')
 // @UseGuards(JwtAuthGuard)
  UpdatePost(@Param('id') id: number, @Body() post: PostDto) {
    return this.postService.update(id, post)
  }

  @Get('')
  GetAllPost() {
    return this.postService.get();
  }

}
