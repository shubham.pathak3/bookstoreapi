import { Body, Controller, DefaultValuePipe, Delete, Get, HttpException, HttpStatus, Param, ParseIntPipe, Post, Put, Query, Res, Scope, UseGuards } from '@nestjs/common';
import { catchError, map, Observable } from 'rxjs';
import { BookDto } from './book.dto';
import { BookService } from './book.service';
import { Response } from 'express';
import { Book } from 'src/database/book.model';
import { ParseObjectIdPipe } from 'src/shared/pipe/parse-object-id.pipe';
import { UpdateBookDto } from './updateBook.dto';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { User } from 'src/database/user.model';
import { RolesGuard } from 'src/auth/guard/role.guard';
import { HasRoles } from 'src/auth/guard/has-role.decorator';
import { RoleType } from 'src/shared/enum/role-type.enum';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@Controller({ path: 'book', scope: Scope.REQUEST })
@ApiTags('book')
export class BookController {

  constructor(private bookService: BookService) { }

  @Get('')
  getAll(@Query('q') keyword?: string,
    @Query('limit', new DefaultValuePipe(10), ParseIntPipe) limit?: number,
    @Query('skip', new DefaultValuePipe(0), ParseIntPipe) skip?: number,
  ): Promise<Book[]> {

    return this.bookService.findAll();
  }
  @Get(':id')
  getBookById(@Param('id', ParseObjectIdPipe) id: string): Promise<Book> {
    return this.bookService.findById(id);
  }

  @Post('')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HasRoles(RoleType.ADMIN)
  AddBook(
    @Body() data: BookDto
  ): Promise<Book> {

    return this.bookService.save(data)
  }

  @Put(':id')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HasRoles(RoleType.ADMIN)
  updateBook(
    @Param('id', ParseObjectIdPipe) id: string,
    @Body() post: UpdateBookDto,
  ): Promise<Book> {
    return this.bookService.update(id, post)
  }

  @Put(':id/addToCart')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard)
  addToCart(
    @Param('id', ParseObjectIdPipe) id: string
  ): Promise<User> {
    return this.bookService.addToCart(id)
  }

  @Put(':id/removeFromCart')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard)
  removeFromCart(
    @Param('id', ParseObjectIdPipe) id: string,
  ): Promise<User> {
    return this.bookService.removeFromCart(id)
  }

  @Put(':id/purcheseBook')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard)
  purcheseBook(
    @Param('id', ParseObjectIdPipe) id: string,

  ): Promise<User> {
    return this.bookService.purcheseBook(id)
  }


  @Delete(':id')
  @ApiBearerAuth('Authorization')
  @UseGuards(JwtAuthGuard, RolesGuard)
  @HasRoles(RoleType.ADMIN)
  deletePostById(
    @Param('id', ParseObjectIdPipe) id: string,
  ): Promise<any> {
    return this.bookService.deleteById(id)
  }


}
