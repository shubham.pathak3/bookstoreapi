import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { BookController } from './book.controller';
import { BookService } from './book.service';

@Module({
    imports: [DatabaseModule],
    controllers: [BookController],
    providers: [BookService],
  })
export class BookModule {}
