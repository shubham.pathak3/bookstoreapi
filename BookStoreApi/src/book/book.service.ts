import { Inject, Injectable, NotFoundException } from '@nestjs/common';
import { EMPTY, from, mergeMap, Observable, of, throwIfEmpty } from 'rxjs';
import { Book } from 'src/database/book.model';
import { BookDto } from './book.dto';
import { Model } from 'mongoose';
import { BOOK_MODEL, USER_MODEL } from '../database/database.constants';
import { REQUEST } from '@nestjs/core';
import { UpdateBookDto } from './updateBook.dto';
import { AuthenticatedRequest } from 'src/auth/interface/authenticated-request';
import { User, UserModel } from 'src/database/user.model';

@Injectable()
export class BookService {

  constructor(
    @Inject(BOOK_MODEL) private bookModel: Model<Book>,
    @Inject(USER_MODEL) private userModel: UserModel,
    @Inject(REQUEST) private req: AuthenticatedRequest,
  ) { }

  async findAll(keyword?: string, skip = 0, limit = 10): Promise<Book[]> {
    if (keyword) {
      return await this.bookModel
        .find({ BookName: { $regex: '.*' + keyword + '.*' } })
        .skip(skip)
        .limit(limit)
        .exec()

    } else {

      return await this.bookModel.find({}).skip(skip).limit(limit).exec();
    }
  }

  async save(data: BookDto): Promise<Book> {
    return await this.bookModel.create({
      ...data,
      createdBy: { _id: this.req.user.id },
    });
  }

  async findById(id: string): Promise<Book> {
    return await this.bookModel.findOne({ _id: id }).exec()
  }

  async addToCart(id: string): Promise<User> {
    return await this.userModel.findOneAndUpdate({ _id: this.req.user.id }, { $push: { cartItems: id } }).exec()

  }
  async removeFromCart(id: string): Promise<User> {
    return await this.userModel.findOneAndUpdate({ _id: this.req.user.id }, { $pull: { cartItems: id } })

  }

  async purcheseBook(id: string): Promise<User> {
    return await this.userModel.findOneAndUpdate({ _id: this.req.user.id }, { $push: { myOrders: id } }).exec()

  }

  async update(id: string, data: UpdateBookDto): Promise<Book> {
    return await this.bookModel.findOneAndUpdate(
      { _id: id },
      { ...data, updatedBy: { _id: this.req.user.id } },
      { new: true },
    )
      .exec()


  }

  async deleteById(id: string): Promise<Book> {
    return await this.bookModel.findOneAndDelete({ _id: id }).exec()
  }


}
