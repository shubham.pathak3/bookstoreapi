import { Inject, Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { EMPTY, from, map, mergeMap, Observable, of, throwIfEmpty } from 'rxjs';
import { USER_MODEL } from 'src/database/database.constants';
import { User, UserModel } from 'src/database/user.model';
import { RoleType } from 'src/shared/enum/role-type.enum';

import { RegisterDto } from './register.dto';


@Injectable()
export class UserService {

  constructor(
    @Inject(USER_MODEL) private userModel: UserModel,
  ) { }

  async register(data: RegisterDto): Promise<User> {

    const created = await this.userModel.create({
      ...data,
      role: RoleType.USER,
    });

    return created;
  }

  async existsByEmail(email: string): Promise<any> {
    return await this.userModel.exists({ email }).exec()
  }
  findByEmail(email: string): Promise<User> {
    const userQuery = this.userModel.findOne({ email: email });
    return this.userModel.findOne({ email }).exec()
  }

  async findByEmaila(email: string): Promise<User> {
    const userQuery = await this.userModel.findOne({ email: email }).exec();

    return userQuery;
  }

  async findById(id: string, cartItem = true): Promise<User> {
    const userQuery = this.userModel.findOne({ _id: id });
    if (cartItem) {
      userQuery.populate('Books').populate("Orders");
    }
    return await userQuery.exec()
  }

  async findAll(): Promise<User[]> {

    return await this.userModel.find({}).exec();

  }



}
