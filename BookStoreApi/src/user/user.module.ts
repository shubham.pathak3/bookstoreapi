import { Module } from '@nestjs/common';
import { DatabaseModule } from 'src/database/database.module';
import { UserController } from './user.controller';
import { UserService } from './user.service';



@Module({
    imports: [DatabaseModule],
    controllers: [UserController],
    exports: [UserService],
    providers: [UserService],
})
export class UserModule { }
