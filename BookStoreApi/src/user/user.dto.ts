export class UserDto {
    readonly id: string;
    readonly email: string;
    readonly password: string;
    readonly firstName?: string;
    readonly lastName?: string;
    readonly createdAt?: Date;
    readonly updatedAt?: Date;
  }