import { Body, ConflictException, Controller, DefaultValuePipe, Get, Param, Post, Put, Query, Req, Res, UseGuards } from '@nestjs/common';
import { map, mergeMap, Observable } from 'rxjs';
import { RegisterDto } from './register.dto';
import { UserService } from './user.service';
import { Response } from 'express';
import { ParseObjectIdPipe } from 'src/shared/pipe/parse-object-id.pipe';
import { User } from 'src/database/user.model';
import { JwtAuthGuard } from 'src/auth/guard/jwt-auth.guard';
import { RolesGuard } from 'src/auth/guard/role.guard';
import { RoleType } from 'src/shared/enum/role-type.enum';
import { HasRoles } from 'src/auth/guard/has-role.decorator';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';


@Controller('user')
@ApiTags('user')
export class UserController {
    constructor(private userService: UserService) { }

    @Post('register')
    async register(
        @Body() registerDto: RegisterDto): Promise<any> {
        const email = registerDto.email;
        let exists = await this.userService.existsByEmail(email)
        if (exists) {
            throw new ConflictException(`email:${email} is existed`)
        } else {
            return this.userService.register(registerDto)
        }

    }

    @Get(':id')
    getUser(
        @Param('id', ParseObjectIdPipe) id: string,
        @Query('cartItem', new DefaultValuePipe(false)) cartItem?: boolean
    ): Promise<Partial<User>> {
        return this.userService.findById(id, cartItem);
    }

    @Get('')
    @ApiBearerAuth('Authorization')
    @UseGuards(JwtAuthGuard, RolesGuard)
    @HasRoles(RoleType.ADMIN)
    getAll(
    ): Promise<User[]> {
      return this.userService.findAll();
    }




}
