import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { DatabaseModule } from './database/database.module';
import { BookController } from './book/book.controller';
import { BookModule } from './book/book.module';
import { UserController } from './user/user.controller';
import { UserService } from './user/user.service';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [DatabaseModule, BookModule, UserModule, AuthModule],
  controllers: [AppController, ],
  providers: [AppService, ],
})
export class AppModule {}
