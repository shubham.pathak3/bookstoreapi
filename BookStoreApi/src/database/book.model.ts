import { Connection, Document, Model, Schema, SchemaTypes } from 'mongoose';
import { User } from './user.model';


interface Book extends Document {
  
  readonly BookName:string;
  readonly Author:string;
  readonly Price:string;
  readonly IsPublished:boolean;
  readonly createdBy?: Partial<User>;
  readonly updatedBy?: Partial<User>;

}

type BookModel = Model<Book>;

const BookSchema = new Schema<Book>(
  {
    BookName: { type: SchemaTypes.String, required: true },
    Author: { type: SchemaTypes.String, required: true },
    Price: { type: SchemaTypes.String, required: true },
    IsPublished: { type: SchemaTypes.Boolean, required: true },
    createdBy: { type: SchemaTypes.ObjectId, ref: 'User', required: false },
    updatedBy: { type: SchemaTypes.ObjectId, ref: 'User', required: false },
  },
  { timestamps: true },
);

const createBookModel: (conn: Connection) => BookModel = (
  connection: Connection,
) => connection.model<Book>('Book', BookSchema, 'Books');

export { Book, BookModel, createBookModel };