import { ConfigType } from '@nestjs/config';
import { Connection, createConnection } from 'mongoose';
import mongodbConfig from 'src/config/mongodb.config';


export const databaseConnectionProviders = [
  {
    provide: "DATABASE_CONNECTION",
    useFactory: (dbConfig: ConfigType<typeof mongodbConfig>): Connection => {
      const conn = createConnection(dbConfig.uri, {
      });

      

      return conn;
    },
    inject: [mongodbConfig.KEY],
  },
];