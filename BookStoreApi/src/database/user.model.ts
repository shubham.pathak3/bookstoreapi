import { Post } from '@nestjs/common';
import { compare, hash } from 'bcrypt';
import { Connection, Document, Model, Schema, SchemaTypes } from 'mongoose';
import { from, Observable } from 'rxjs';
import { RoleType } from 'src/shared/enum/role-type.enum';
import { Book } from './book.model';

interface User extends Document {
  comparePassword(password: string): Promise<boolean>;
  readonly email: string;
  readonly password: string;
  readonly firstName?: string;
  readonly lastName?: string;
  readonly cartItems?: Partial<Book[]>;
  readonly myOrders?: Partial<Book[]>;
  readonly role?:RoleType

}   

type UserModel = Model<User>;

const UserSchema = new Schema<User>(
  {
    password: { type: SchemaTypes.String, required: true },
    email: { type: SchemaTypes.String, required: true },
    firstName: { type: SchemaTypes.String, required: true },
    lastName: { type: SchemaTypes.String, required: false },
    cartItems:[{ type: SchemaTypes.ObjectId, ref: 'Book', required: false }],
    myOrders:[{ type: SchemaTypes.ObjectId, ref: 'Book', required: false }],
    role: { type: SchemaTypes.String, enum: ['ADMIN', 'USER'], required: false },
    
  },
  {
    timestamps: true,
    toJSON: {
      virtuals: true,
    },
  },
);

async function preSaveHook(next) {
  if (!this.isModified('password')) return next();

  const password = await hash(this.password, 12);
  this.set('password', password);

  next();
}

UserSchema.pre<User>('save', preSaveHook);

function comparePasswordMethod(password: string): Promise<any> {
  return compare(password, this.password);
}

UserSchema.methods.comparePassword = comparePasswordMethod;

UserSchema.virtual('Books', {
  ref: 'Book',
  localField: 'cartItems',
  foreignField: '_id',            
});
UserSchema.virtual('Orders', {
    ref: 'Book',
    localField: 'myOrders',
    foreignField: '_id',            
  });

const createUserModel: (conn: Connection) => UserModel = (conn: Connection) =>
  conn.model<User>('User', UserSchema, 'users');

export {
  User,
  UserModel,
  createUserModel,
  UserSchema,
  preSaveHook,
  comparePasswordMethod,
};