import { Connection } from 'mongoose';
import { Book, createBookModel } from './book.model';
import {
  DATABASE_CONNECTION,
  BOOK_MODEL,
  USER_MODEL
} from './database.constants';
import { createUserModel } from './user.model';


export const databaseModelsProviders = [
  {
    provide: BOOK_MODEL,
    useFactory: (connection: Connection) => createBookModel(connection),
    inject: [DATABASE_CONNECTION],
  },
  {
    provide: USER_MODEL,
    useFactory: (connection: Connection) => createUserModel(connection),
    inject: [DATABASE_CONNECTION],
  },
  
];