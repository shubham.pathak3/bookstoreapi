import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { EMPTY, from, Observable, of } from 'rxjs';
import { mergeMap, map, throwIfEmpty } from 'rxjs/operators';
import { User } from 'src/database/user.model';
import { UserService } from '../user/user.service';
import { AccessToken } from './interface/access-token';
import { JwtPayload } from './interface/jwt-payload';
import { UserPrincipal } from './interface/user-principal';

@Injectable()
export class AuthService {
  constructor(
    private userService: UserService,
    private jwtService: JwtService,
  ) { }


  login(user: UserPrincipal): Observable<AccessToken> {
    const payload: JwtPayload = {
      sub: user.id,
      email: user.email,
      role:user.role
    
    };
    return from(this.jwtService.signAsync(payload)).pipe(
      map((access_token) => {
        return { access_token };
      }),
    );
  }


  async validateUser(email: string, password: string): Promise<User> {
    let user: User;

    try {
      user = await this.userService.findByEmaila( email );
    } catch (err) {
      throw new UnauthorizedException(
        `There isn't any user with email: ${email}`,
      );
    }
    if (!(await user.comparePassword(password))) {
      throw new UnauthorizedException(
        `Wrong password for user with email: ${email}`,
      );
    }
 

    return user;
  }
}