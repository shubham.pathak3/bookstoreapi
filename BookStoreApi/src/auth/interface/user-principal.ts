export interface UserPrincipal {
    readonly id: string;
    readonly email: string;
    readonly role:string;
  
  }