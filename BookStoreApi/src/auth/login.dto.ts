import { ApiProperty } from '@nestjs/swagger';
import { IsEmail, IsNotEmpty } from 'class-validator';
import { UserPrincipal } from './interface/user-principal';
export class LoginDto {

  @IsNotEmpty()
  @ApiProperty()
  readonly username: string;
  @IsNotEmpty()
  @ApiProperty()
  readonly password: string;
  readonly user : UserPrincipal
  
}