import { Body, Controller, Post,Req,Request, Res, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { Response } from 'express';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/database/user.model';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guard/jwt-auth.guard';
import { LocalAuthGuard } from './guard/local-auth.guard';
import { AuthenticatedRequest } from './interface/authenticated-request';
import { Login } from './interface/login';
import { LoginDto } from './login.dto';


@Controller('auth')
@ApiTags('auth')
export class AuthController {
  constructor(private authService: AuthService) { }

 @UseGuards(LocalAuthGuard)
  @Post('login')
  login(@Body() req:LoginDto,@Req() re, @Res() res: Response): Observable<Response> {
    return this.authService.login(re.user)   
      .pipe(
        map(token => {
          return res
            .header('Authorization', 'Bearer ' + token.access_token)
            .json(token)
            .send()
        })
      );
  }
}